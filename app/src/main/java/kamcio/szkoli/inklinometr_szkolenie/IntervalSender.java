package kamcio.szkoli.inklinometr_szkolenie;

import android.provider.ContactsContract;

public class IntervalSender implements Runnable {

    private ActivityMain activityMain;
    private DatabaseManager databaseManager;
    private SensorReader sensorReader;
    private int delay;
    private int measurementLength;

    IntervalSender(ActivityMain activity, DatabaseManager databaseManager,SensorReader sensorReader, int delay, int measurementLength){
        this.activityMain = activity;
        this.databaseManager = databaseManager;
        this.sensorReader = sensorReader;
        this.delay = delay;
        this.measurementLength = measurementLength;
    }


    @Override
    public void run() {
        int numberOfIterations = measurementLength/delay;
        int i = 0;
        while(i < numberOfIterations) {
            activityMain.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activityMain.azimuthText.setText(sensorReader.azimuthToText());
                    activityMain.rollText.setText(sensorReader.rollToText());
                    activityMain.pitchText.setText(sensorReader.pitchToText());
                }
            });
            databaseManager.sendNewMeasurement(sensorReader.getMeasurements());
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
