package kamcio.szkoli.inklinometr_szkolenie;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorReader {

    // Gravity rotational data
    private float gravity[];
    // Magnetic rotational data
    private float magnetic[]; //for magnetic rotational data
    private float accels[] = new float[3];
    private float mags[] = new float[3];
    private float[] values = new float[3];

    // azimuth, pitch and roll
    private float azimuth;
    private float pitch;
    private float roll;

    private SensorEventListener sensorEventListener;

    private final float RAD_TO_DEG = 57.2957795f;

    SensorReader(Object sensorService){
        SensorManager sensorManager = (SensorManager)sensorService;
        startEventListener();
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),SensorManager.SENSOR_DELAY_NORMAL);
    }

    public float[] getMeasurements(){
        return new float[]{azimuth,pitch,roll};
    }

    public String azimuthToText(){
        return String.valueOf(azimuth);
    }

    public String rollToText(){
        return String.valueOf(roll);
    }

    public String pitchToText(){
        return String.valueOf(pitch);
    }

    private void startEventListener(){
        sensorEventListener = new SensorEventListener() {
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

            public void onSensorChanged(SensorEvent event) {
                switch (event.sensor.getType()) {
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        mags = event.values.clone();
                        break;
                    case Sensor.TYPE_ACCELEROMETER:
                        accels = event.values.clone();
                        break;
                }

                if (mags != null && accels != null) {
                    gravity = new float[9];
                    magnetic = new float[9];
                    SensorManager.getRotationMatrix(gravity, magnetic, accels, mags);
                    float[] outGravity = new float[9];
                    SensorManager.remapCoordinateSystem(gravity, SensorManager.AXIS_X, SensorManager.AXIS_Z, outGravity);
                    SensorManager.getOrientation(outGravity, values);
                    azimuth = values[0] * RAD_TO_DEG;
                    pitch = values[1] * RAD_TO_DEG;
                    roll = values[2] * RAD_TO_DEG;
                    mags = null;
                    accels = null;
                }
            }
        };
    }



}
