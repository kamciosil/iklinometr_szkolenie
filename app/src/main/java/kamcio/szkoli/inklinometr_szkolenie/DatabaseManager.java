package kamcio.szkoli.inklinometr_szkolenie;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DatabaseManager {
    private FirebaseDatabase database;
    private DatabaseReference inertialValues;

    public DatabaseManager(){
        database = FirebaseDatabase.getInstance();
        inertialValues = database.getReference("InertialValues");
    }

    public void sendNewMeasurement(float []measurements){
        long curTimeStamp = System.currentTimeMillis();
        inertialValues.child(String.valueOf(curTimeStamp)).child("Azimuth").setValue(measurements[0]);
        inertialValues.child(String.valueOf(curTimeStamp)).child("Pitch").setValue(measurements[1]);
        inertialValues.child(String.valueOf(curTimeStamp)).child("Roll").setValue(measurements[2]);
    }
}
