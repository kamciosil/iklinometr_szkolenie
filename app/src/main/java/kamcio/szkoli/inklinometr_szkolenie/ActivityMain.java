package kamcio.szkoli.inklinometr_szkolenie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityMain extends AppCompatActivity {

    private ActivityMain a = this;
    public TextView azimuthText;
    public TextView pitchText;
    public TextView rollText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DatabaseManager databaseManager = new DatabaseManager();
        final SensorReader sensorReader = new SensorReader(getSystemService(SENSOR_SERVICE));

        azimuthText = findViewById(R.id.azimuthValue);
        pitchText = findViewById(R.id.pitchValue);
        rollText = findViewById(R.id.rollValue);
        Button sendToBaseButton = findViewById(R.id.sendToBaseButton);
        sendToBaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseManager.sendNewMeasurement(sensorReader.getMeasurements());
            }
        });

        Button updateTextButton = findViewById(R.id.updateTextButton);
        updateTextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                azimuthText.setText(sensorReader.azimuthToText());
                rollText.setText(sensorReader.rollToText());
                pitchText.setText(sensorReader.pitchToText());
            }
        });


        final IntervalSender intervalSender = new IntervalSender(this,databaseManager,sensorReader,5000,20000);

        final Button intervalSendButton = findViewById(R.id.IntervalSendButton);
        intervalSendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(intervalSender);
                thread.start();
            }
        });
    }
}
